
为了减小仓库体积加快下载，BSP源码采取分散放置的方式。    
你需要单独下载所需仓库放入此文件夹下。如果放在了    
其他路径，请更改BSP下SConstruct文件中"RTT_ROOT"的值(内核源码位置)。    
如果你想分享自己的BSP可以追加信息到下面列表中。

|芯片或板卡厂商|芯片型号|仓库|
| -- | -- | -- |
| st | stm32xx | https://gitee.com/heyuanjie87/bsp-stm32.git |
| kendryte | k210 | https://gitee.com/heyuanjie87/bsp-k210.git |

