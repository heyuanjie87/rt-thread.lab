#include <rtthread.h>
#include <dfs_posix.h>
#include <stdio.h>
#include <stdlib.h>

static int _cat(const char* filename)
{
    int length;
    char buffer[81];
    int fd;

    fd = open(filename, O_RDONLY, 0);
    if (fd < 0)
    {
        printf("Open %s failed\n", filename);
        return -1;
    }

    do
    {
        memset(buffer, 0, sizeof(buffer));
        length = read(fd, buffer, sizeof(buffer)-1);
        if (length > 0)
        {
            printf("%s", buffer);
            fflush(stdout);
        }
    }while (length > 0);

    close(fd);
    printf("\n");

    return 0;
}

static int cat(int argc, char **argv)
{
    int index;

    if (argc == 1)
    {
        printf("Usage: cat [FILE]...\n");
        printf("Concatenate FILE(s)\n");
        return 0;
    }

    for (index = 1; index < argc; index ++)
    {
        _cat(argv[index]);
    }

    return 0;
}
MSH_CMD_EXPORT(cat, Concatenate FILE(s));

static int rm(int argc, char **argv)
{
    int index;

    if (argc == 1)
    {
        printf("Usage: rm FILE...\n");
        printf("Remove (unlink) the FILE(s).\n");
        return 0;
    }

    for (index = 1; index < argc; index ++)
    {
        unlink(argv[index]);
    }

    return 0;
}
MSH_CMD_EXPORT(rm, Remove(unlink) the FILE(s).);

static int echo(int argc, char** argv)
{
    int fd;

    if (argc == 2)
    {
        printf("%s\n", argv[1]);
        return 0;
    }

    if (argc == 3)
    {
        fd = open(argv[2], O_RDWR | O_APPEND | O_CREAT, 0);
        if (fd < 0)
        {
            printf("open file:%s failed!\n", argv[2]);
            return -1;
        }

        write (fd, argv[1], strlen(argv[1]));
        close(fd);
    }
    else if (argc == 4)
    {
        char *str;
        char *buf;
        int len = 0;

        if (strcmp(argv[1], "-e"))
        {
            printf("option not surport\n");
            return -1;
        }

        str = argv[2];
        buf = malloc(strlen(str));
        if (!buf)
        {
            printf("no mem\n");
            return -1;
        }

        fd = open(argv[3], O_RDWR | O_APPEND | O_CREAT, 0);
        if (fd < 0)
        {
            printf("open file:%s failed!\n", argv[3]);
            return -1;
        }

        while (*str)
        {
            if (*str != '\\')
            {
                buf[len++] = *str;
                str ++;
                continue;
            }

            str ++;
            switch(*str)
            {
            case 'r':
                buf[len++] = '\r';
                break;
            case 'n':
                buf[len++] = '\n';
                break;
            case '\\':
                buf[len++] = '\\';
                break;
            case '"':
                buf[len++] = '"';
                break;
            }
            str++;
        }

        write(fd, buf, len);
        close(fd);
        free(buf);
    }
    else
    {
        printf("Usage error\n");
    }

    return 0;
}
MSH_CMD_EXPORT(echo, echo string to file);

static int cmd_mkdir(int argc, char **argv)
{
    if (argc == 1)
    {
        printf("Usage: mkdir [OPTION] DIRECTORY\n");
        printf("Create the DIRECTORY, if they do not already exist.\n");
    }
    else
    {
        mkdir(argv[1], 0);
    }

    return 0;
}
MSH_CMD_EXPORT_ALIAS(cmd_mkdir, mkdir, Create the DIRECTORY.);

static int cmd_mkfs(int argc, char **argv)
{
    int result = 0;
    char *type = "elm"; /* use the default file system type as 'fatfs' */

    if (argc == 2)
    {
        result = dfs_mkfs(type, argv[1]);
    }
    else if (argc == 4)
    {
        if (strcmp(argv[1], "-t") == 0)
        {
            type = argv[2];
            result = dfs_mkfs(type, argv[3]);
        }
    }
    else
    {
        printf("Usage: mkfs [-t type] device\n");
        return 0;
    }

    if (result != RT_EOK)
    {
        printf("mkfs failed, result=%d\n", result);
    }

    return 0;
}
MSH_CMD_EXPORT_ALIAS(cmd_mkfs, mkfs, format disk with file system);

static int _ls(const char *path)
{
    struct stat st;
    DIR *dir;
    struct dirent *dirent;
    int ret = -1;

    /* list directory */
    dir = opendir(path);
    if (dir != 0)
    {
        printf("Directory %s:\n", path);
        do
        {
            dirent = readdir(dir);

            if (dirent != 0)
            {
                char* fn;
                int n;

                n = strlen(path) + dirent->d_namlen + 2;
                fn =  rt_malloc(n);
                memset(&stat, 0, sizeof(struct stat));
                snprintf(fn, n, "%s/%s", path, dirent->d_name);

                if (stat(fn, &st) == 0)
                {
                    printf("%-20s", dirent->d_name);
                    if (S_ISDIR(st.st_mode))
                    {
                        printf("%-25s\n", "<DIR>");
                    }
                    else
                    {
                        printf("%-25lu\n", st.st_size);
                    }
                }
                else
                    printf("BAD file: %s\n", dirent->d_name);
                rt_free(fn);

                ret = 0;
            }
        }while(dirent != 0);

        closedir(dir);
    }
    else
    {
        printf("No such directory\n");
    }

		return ret;
}

static int cmd_ls(int argc, char **argv)
{
    if (argc == 1)
    {
        _ls("/");
    }
    else
    {
        _ls(argv[1]);
    }

    return 0;
}
MSH_CMD_EXPORT_ALIAS(cmd_ls, ls, List information about the FILEs.);

static int cmd_mount(int argc, char **argv)
{
    int ret = -1;
    char *type = 0;
    char *dev = 0;
    char *path = 0;
    int rw = 0;
    void *d = 0;
 
    if (argc < 5)
    {
        printf("Usage: mount -t fs device path\n");
        return -1;
    }

    if (strcmp(argv[1], "-t") == 0)
    {
        type = argv[2];
        dev = argv[3];
        path = argv[4];
    }

    if (dev && path && type)
        ret = dfs_mount(dev, path, type, rw, d);

    if (ret != 0)
        printf("mount failed: %d\n", ret);

    return ret;
}
MSH_CMD_EXPORT_ALIAS(cmd_mount, mount, Mount a filesystem);

static int cmd_lsdev(int argc, char **argv)
{
    return _ls(":dev");
}
MSH_CMD_EXPORT_ALIAS(cmd_lsdev, lsdev, List devices);

static int cmd_lsmem(int argc, char **argv)
{
    return _cat(":proc/meminfo");
}
MSH_CMD_EXPORT_ALIAS(cmd_lsmem, lsmem, List RAM infomation);

static int cmd_lsthread(int argc, char **argv)
{
    return _cat(":proc/thread");
}
MSH_CMD_EXPORT_ALIAS(cmd_lsthread, lsthread, List thread infomation);

static int hexdump(int argc, char **argv)
{
    char buf[16];
    int fd;
    int len;

    if (argc < 2)
    {
        printf("Usage: hexdump FILE\n");
        return -1;
    }

    fd = open(argv[1], O_RDONLY);
    if (fd < 0)
    {
        printf("open fail\n");
        return -1;
    }

    do
    {
        len = read(fd, buf, sizeof(buf));
        if (len > 0)
        {
            int i;

            for (i = 0; i < len; i ++)
            {
                printf("%02X ", buf[i]);
            }
            printf("\n");
        }
    } while(len > 0);
    close(fd);

    return 0;
}
MSH_CMD_EXPORT(hexdump, hexdump file);
