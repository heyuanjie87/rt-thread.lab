#include <dfs.h>
#include <dfs_file.h>
#include <dfs_private.h>
#include <dfs_poll.h>

static int _hungup_open(struct dfs_fd *fd, ...)
{
    return -EIO;
}

static int _hungup_close(struct dfs_fd *fd)
{
    return 0;
}

static int _hungup_ioctl(struct dfs_fd *fd, int cmd, void *args)
{
    return -EIO;
}

static int _hungup_read(struct dfs_fd *fd, void *buf, size_t count, ...)
{
    return 0;
}

static int _hungup_write(struct dfs_fd *fd, const void *buf, size_t count, ...)
{
    return -EIO;
}

static int _hungup_flush(struct dfs_fd *fd)
{
    return -EIO;
}

static int _hungup_lseek(struct dfs_fd *fd, off_t offset, int whence)
{
    return -ESPIPE;
}

static int _hungup_getdents(struct dfs_fd *fd, struct dirent *dirp, uint32_t count)
{
    return 0;
}

static int _hungup_poll(struct dfs_fd *fd, struct rt_pollreq *req)
{
    return (POLLHUP | POLLERR | POLLIN);
}

const static struct dfs_file_ops _hungup_fops =
{
    _hungup_open,
    _hungup_close,
    _hungup_ioctl,
    _hungup_read,
    _hungup_write,
    _hungup_flush, /* flush */
    _hungup_lseek, /* lseek */
    _hungup_getdents, /* getdents */
    _hungup_poll,
};

void dfs_file_hungup(struct dfs_fd *fd)
{
    fd->fops = &_hungup_fops;
}
