#ifndef _MQPOSIX_H
#define _MQPOSIX_H    1

#ifdef __cplusplus
extern "C" {
#endif
struct mq_attr;

int sys_mq_open(const char *name, int oflag, int mode, struct mq_attr *attr);
int sys_mq_unlink(const char *path);

#ifdef __cplusplus
}
#endif

#endif
