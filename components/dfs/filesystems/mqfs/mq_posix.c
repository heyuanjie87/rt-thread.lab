#include <dfs.h>
#include <dfs_fs.h>
#include <dfs_file.h>

#include <mqueue.h>

#include "mq_posix.h"

int sys_mq_open(const char *name, int oflag, int mode, struct mq_attr *attr)
{
    int fd, result;
    struct dfs_fd *d;

    /* allocate a fd */
    fd = fd_new();
    if (fd < 0)
    {
        return -ENOMEM;
    }
    d = fd_get(fd);

    result = dfs_file_open_fspath(d, name, oflag, ":mq", mode, attr);
    if (result < 0)
    {
        /* release the ref-count of fd */
        fd_put(d);
        fd_put(d);

        return result;
    }

    /* release the ref-count of fd */
    fd_put(d);

    return fd;
}

int sys_mq_unlink(const char *path)
{
    int result;
    struct dfs_filesystem *fs;

    /* get filesystem */
    if ((fs = dfs_filesystem_lookup(":mq")) == NULL)
    {
        result = -ENOENT;
        goto __exit;
    }

    if (!fs->ops->unlink)
        result = fs->ops->unlink(fs, path);
    else 
        result = -ENOSYS;

__exit:
    return result;
}
