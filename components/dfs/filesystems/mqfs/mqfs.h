#ifndef _MQFS_H_
#define _MQFS_H_

#include <rtthread.h>
#include <stdint.h>
#include <mqueue.h>

struct mqfs_dirent
{
    rt_list_t dlist;
    rt_list_t mlist;
    struct rt_mutex lock;
    char name[14];
    uint16_t maxlen;
    uint32_t maxmsgs;
    uint16_t curmsgs;
    uint8_t ref_cnt;
    uint8_t isdel;
    rt_wqueue_t rwq;
    rt_wqueue_t wwq;
};

struct mqfs_msg
{
    rt_list_t node;
    void *msg;
    uint32_t len;
    uint32_t prio;
};

struct mqfs_dirent* mqfs_dirent_alloc(const char *name, int mode, struct mq_attr *attr);
int mqfs_open(struct mqfs_dirent *root, const char *name, int oflag, 
                     int mode, struct mq_attr *attr, struct mqfs_dirent **dst);
int mqfs_read(struct mqfs_dirent *file, void *buf, size_t count, unsigned *prio);
int mqfs_write(struct mqfs_dirent *file, const void *buf, size_t count, unsigned prio);

#endif
