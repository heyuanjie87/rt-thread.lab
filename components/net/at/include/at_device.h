#ifndef _AT_DEVICE_H
#define _AT_DEVICE_H

#include <rtdevice.h>
#include <netdev_ipaddr.h>
#include <netdev.h>

#ifdef __cplusplus
extern "C" {
#endif

#define ATDIOC_MKNETDEV  _IO('A', 10)
#define ATDIOC_RESP      _IOW('A', 11, struct at_resp)
#define ATDIOC_QUERYBUF  _IOWR('A', 12, struct at_buffer)

struct at_socket_ops;

enum at_event_type
{
    AT_EV_OSK = 1,
    AT_EV_DOUT,
    AT_EV_DIN,
    AT_EV_CLOSE,
    AT_EV_DMRV,
};

struct at_resp
{
    enum at_event_type type;
    int id;
    int status;
    void *data;
};

struct at_buffer
{
    enum at_event_type type;
    int id;
    char *data;
    int length;
};

struct at_device
{
    struct rt_device parent;

    struct at_socket *sockets;
    int socket_num;

    struct rt_mutex *cmdlock;
    struct rt_ringbuffer *cmd_req;
    struct rt_ringbuffer *cmd_rsp;
    rt_wqueue_t wq_req;
    rt_wqueue_t wq_rsp;

    rt_list_t reqs_cmdhead;
    rt_list_t reqs_dathead;
    rt_list_t idle_dathead;

    struct netdev *netdev;
    const struct at_socket_ops *socket_ops;
    int (*at_domain_resolve)(struct at_device *atd, const char *name, char ip[16]);
};

int rt_at_device_register(struct at_device *dev, const char *name);

#ifdef __cplusplus
}
#endif

#endif
