#include <stddef.h>
#include <stdlib.h>
#include <string.h>

char *strndup(const char *str, size_t len)
{
    char *dst;
    size_t slen = strlen(str);

    if (len > slen)
        len = slen;

    dst = malloc(len + 1);
    if (dst)
    {
        strncpy(dst, str, len);
        dst[len] = 0;
    }

    return dst;
}
