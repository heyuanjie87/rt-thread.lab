#ifndef _MQUEUE_H
#define _MQUEUE_H    1

#include <stdint.h>

typedef int mqd_t;
struct mq_attr
{
    uint32_t mq_flags;	/* Message queue flags.  */
    uint32_t mq_msgsize;	/* Maximum message size.  */
    uint32_t mq_maxmsg;	/* Maximum number of messages.  */
    uint32_t mq_curmsgs;	/* Number of messages currently queued.  */
};

#ifdef __cplusplus
extern "C" {
#endif

mqd_t mq_open(const char *name, int oflag, ...);
int mq_close(mqd_t mqdes);
int mq_unlink(const char *name);
int mq_receive(mqd_t mqdes, char *msg_ptr, size_t msg_len, unsigned *msg_prio);
int mq_send(mqd_t mqdes, const char *msg_ptr, size_t msg_len, unsigned msg_prio);

#ifdef __cplusplus
}
#endif

#endif
