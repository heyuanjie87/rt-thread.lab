#ifndef __UAPI_DEV_ADC_H
#define __UAPI_DEV_ADC_H

#include <stdint.h>

#define ADC_GETINFO    0x0801

struct rt_adc_info
{
    uint8_t channels;     /* minimum 1 */
    uint8_t flags;
    int32_t sampling_max; /* 最大采集值 */
    int32_t sampling_min; /* 最小采集值 */
    float vref;           /* reference voltage */
};

#endif
