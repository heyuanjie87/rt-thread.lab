#ifndef __UAPI_DEV_PWM_H
#define __UAPI_DEV_PWM_H

#include <stdint.h>

#define PWM_START       (0x0901)
#define PWM_STOP        (0x0902)
#define PWM_SET         (0x0903)
#define PWM_GET         (0x0904)

struct rt_pwm_configuration
{
    uint32_t period;  /* unit:ns 1ns~4.29s:1Ghz~0.23hz */
    uint32_t pulse;   /* unit:ns (pulse<=period) */
    uint8_t channel; /* 0-n */
    uint8_t enabled; /* 0:stop,1: start */
};

#endif
