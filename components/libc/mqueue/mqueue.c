#include <dfs_posix.h>
#include <mq_posix.h>
#include <mqueue.h>

#include <stdarg.h>

mqd_t mq_open(const char *name, int oflag, ...)
{
    mqd_t ret;
    va_list ap;
    struct mq_attr *attr;
    mode_t mode;

    va_start(ap, oflag);
    mode = va_arg(ap, mode_t);
    attr = va_arg(ap, struct mq_attr *);
    va_end(ap);

    ret = sys_mq_open(name, oflag, mode, attr);

    return ret;
}

int mq_close(mqd_t fd)
{
    return close(fd);
}

int mq_send(mqd_t fd, const char *msg_ptr, size_t msg_len, unsigned msg_prio)
{
    int ret;

    ret = sys_mq_write(fd, msg_ptr, msg_len, msg_prio);

    return ret;
}

int mq_receive(mqd_t fd, char *msg_ptr, size_t msg_len, unsigned *msg_prio)
{
    int ret;

    ret = sys_mq_read(fd, msg_ptr, msg_len, msg_prio);

    return ret;
}

int mq_unlink(const char *path)
{
    int ret;

    ret = sys_mq_unlink(path);

    return ret;
}
