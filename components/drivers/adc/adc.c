/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2021-10-20     heyuanjie87    1st version
 */

#include <rtthread.h>
#include <rtdevice.h>

#include <dfs_posix.h>
#include <dfs_poll.h>

static int _fops_open(struct dfs_fd *file, ...)
{
    int ret = 0;
    rt_adc_t *adc = (rt_adc_t *)file->data;

    adc->ops->init(adc);

    return ret;
}

static int _fops_close(struct dfs_fd *fd)
{
    int ret;

    return ret;
}

static int _fops_ioctl(struct dfs_fd *file, int cmd, void *args)
{
    int ret = 0;
    rt_adc_t *adc = (rt_adc_t *)file->data;

    switch (cmd)
    {
    case ADC_GETINFO:
    {
        struct rt_adc_info *info = (struct rt_adc_info*)args;

        *info = adc->info;
    }
    break;
    default:
    {
        ret = -1;
    }
    break;
    }

    return ret;
}

static int _fops_read(struct dfs_fd *file, void *buf, size_t size, ...)
{
    rt_adc_t *adc = (rt_adc_t *)file->data;
    int32_t val;

    if (size & 0x3)
    {
        return -EINVAL;
    }

    adc->ops->convert(adc, 0, &val);
    rt_memcpy(buf, &val, 4);

    return 4;
}

static const struct dfs_file_ops _adc_fops = {
    _fops_open,
    _fops_close,
    _fops_ioctl,
    _fops_read,
    RT_NULL,
    RT_NULL, /* flush */
    RT_NULL, /* lseek */
    RT_NULL, /* getdents */
    RT_NULL,
};

int rt_hw_adc_register(rt_adc_t *adc, const char *name, void *user_data)
{
    int result;

    adc->parent.type = RT_Device_Class_Char;
    adc->parent.user_data = user_data;

    result = rt_device_register(&adc->parent, name, RT_DEVICE_FLAG_RDWR);
    adc->parent.fops = &_adc_fops;

    return result;
}
