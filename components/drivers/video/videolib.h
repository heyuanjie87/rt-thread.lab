#ifndef __VIDEOLIB_H
#define __VIDEOLIB_H

int rt_video_open(rt_video_t *vid);
int rt_video_close(rt_video_t *vid);
int rt_video_reqbufs(rt_video_t *vid, struct video_requestbuffers *req);
int rt_video_querybuf(rt_video_t *vid, struct video_buffer *buf);
int rt_video_qbuf(rt_video_t *vid, struct video_buffer *buf);
int rt_video_streamon(rt_video_t *vid);
int rt_video_read(rt_video_t *vid, void *buf, size_t size, int nonblock);
int rt_video_s_fmt(rt_video_t *vid, struct video_format *fmt);

int rt_camif_set_xclk(rt_camif_t *ci, uint32_t freq);

#endif
