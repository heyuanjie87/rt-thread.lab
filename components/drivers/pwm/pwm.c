/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2021-10-23     heyuanjie87    1st version
 */

#include <rtthread.h>
#include <rtdevice.h>

#include <dfs_posix.h>
#include <dfs_poll.h>

static int _fops_open(struct dfs_fd *file, ...)
{
    int ret = 0;
    rt_pwm_t *dev = (rt_pwm_t *)file->data;

    dev->ops->init(dev);

    return ret;
}

static int _fops_close(struct dfs_fd *fd)
{
    int ret;

    return ret;
}

static int _fops_ioctl(struct dfs_fd *file, int cmd, void *args)
{
    int ret = 0;
    rt_pwm_t *dev = (rt_pwm_t *)file->data;


    return ret;
}

static int _fops_write(struct dfs_fd *file, void *buf, size_t size, ...)
{

    return 0;
}

static const struct dfs_file_ops _pwm_fops = {
    _fops_open,
    _fops_close,
    _fops_ioctl,
    RT_NULL,
    _fops_write,
    RT_NULL, /* flush */
    RT_NULL, /* lseek */
    RT_NULL, /* getdents */
    RT_NULL,
};

int rt_hw_pwm_register(rt_adc_t *adc, const char *name, void *user_data)
{
    int result;

    adc->parent.type = RT_Device_Class_Char;
    adc->parent.user_data = user_data;

    result = rt_device_register(&adc->parent, name, RT_DEVICE_FLAG_RDWR);
    adc->parent.fops = &_pwm_fops;

    return result;
}
