#ifndef _CDC_VCOM_H_
#define _CDC_VCOM_H_

struct cdc_eps
{
    uep_t ep_out;
    uep_t ep_in;
    uep_t ep_cmd;
};
typedef struct cdc_eps* cdc_eps_t;


#endif
