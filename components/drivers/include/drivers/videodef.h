#ifndef __VIDEODEF_H
#define __VIDEODEF_H

#include <stdint.h>

#define VIDIOC_S_FMT 0x5601
#define VIDIOC_REQBUFS 0x5602
#define VIDIOC_QUERYBUF 0x5603
#define VIDIOC_QBUF    0x5604
#define VIDIOC_DQBUF    0x5605
#define VIDIOC_STREAMON 0x5606
#define VIDIOC_STREAMOFF 0x5607

#define VIDEO_MEMORY_USERPTR 1
#define VIDEO_MEMORY_MMAP    2

enum video_type
{
    VIDEO_TYPE_CAPTURE = 1,
};

#define VIDEO_PIX_FMT_RGB565    (565)

struct video_pix_format
{
    uint32_t width;
    uint32_t height;
    uint32_t pixelformat;
    uint32_t bytesperline;
};

struct video_format
{
    enum video_type type;
    union
    {
        struct video_pix_format pix;
    }fmt;
};

struct video_requestbuffers
{
    uint32_t count;
};

struct video_buffer
{
    enum video_type type;
    uint16_t index;
    uint16_t memory;
    union
    {
        uint32_t offset;
        void *userptr;
    } m;
    uint32_t length;
};

#endif
