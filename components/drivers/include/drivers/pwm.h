/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-05-07     aozima       the first version
 */

#ifndef __DRV_PWM_H_INCLUDE__
#define __DRV_PWM_H_INCLUDE__

#include <rtdef.h>
#include <usdev/pwm.h>

struct rt_device_pwm;
struct rt_pwm_ops
{
    int (*init)(struct rt_device_pwm *pwm);
    void (*deinit)(struct rt_device_pwm *pwm);
    int (*apply)(struct rt_device_pwm *pwm, struct rt_pwm_configuration *cfg);
};

struct rt_device_pwm
{
    struct rt_device parent;
    const struct rt_pwm_ops *ops;
};
typedef struct rt_device_pwm rt_pwm_t;

int rt_hw_pwm_register(struct rt_device_pwm *device, const char *name, void *user_data);

#endif /* __DRV_PWM_H_INCLUDE__ */
