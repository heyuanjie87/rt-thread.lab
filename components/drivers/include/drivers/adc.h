/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-05-07     aozima       the first version
 * 2018-11-16     Ernest Chen  add finsh command and update adc function
 */

#ifndef __ADC_H__
#define __ADC_H__

#include <rtdef.h>

#include <usdev/adc.h>

struct rt_adc_device;

struct rt_adc_ops
{
    int (*init)(struct rt_adc_device *device);
    void (*deinit)(struct rt_adc_device *device);
    /* single convert */
    int (*convert)(struct rt_adc_device *device, uint32_t channel, int32_t *value);
};

struct rt_adc_device
{
    struct rt_device parent;

    const struct rt_adc_ops *ops;
    struct rt_adc_info info;
};
typedef struct rt_adc_device rt_adc_t;

int rt_hw_adc_register(rt_adc_t *adc, const char *name, void *user_data);

#endif /* __ADC_H__ */
