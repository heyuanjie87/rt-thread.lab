#include <rtthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

static int seeksw(int argc, char **argv)
{
    int pos;
    char c = '0';
    int fd;

    if (argc < 3)
    {
        printf("Usage: seeksw FILE POS [CHAR]\n");
        return -1;
    }

    if (argc == 4)
    {
        c = argv[3][0];
    }

    pos = atoi(argv[2]);
    fd = open(argv[1], O_RDWR);
    if (fd < 0)
    {
        printf("open fail\n");
        return -1;
    }

    if (lseek(fd, pos, SEEK_SET) != pos)
    {
        printf("seek fail\n");
        goto _out;
    }

    write(fd, &c, 1);

_out:
    close(fd);

    return 0;
}
MSH_CMD_EXPORT(seeksw, seek set pos and write);
