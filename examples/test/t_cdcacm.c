#include <rtthread.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

static void acmwr(int argc, char **argv)
{
    int fd;
    int cnt = 1;
    char *buf;
    int ret;

    if (argc < 2)
    {
        printf("param err\n");
        return;
    }
    if (argc == 3)
        cnt = atoi(argv[2]);

    if ((buf = rt_malloc(64)) == RT_NULL)
    {
        printf("no mem\n");
        return;
    }

    printf("open %s\n", argv[1]);

    fd = open(argv[1], O_RDWR);
    if (fd < 0)
    {
        printf("open fail\n");
        goto _out;
    }

    while (cnt --)
    {
        ret = write(fd, "AT\r\n", 4);
        if (ret < 0)
        {
            printf("write fail\n");
            break;
        }
        ret = read(fd, buf, 63);
        if (ret <= 0)
        {
            printf("read fail\n");
            break;            
        }

        buf[ret] = 0;
        printf(buf);
        fflush(stdout);
        rt_thread_mdelay(1000);
    }
    close(fd);

_out:
    rt_free(buf);
}
MSH_CMD_EXPORT(acmwr, acmwr devname [times] -- acm Write and Read);
