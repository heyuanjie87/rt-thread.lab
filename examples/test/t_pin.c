#include <rtthread.h>
#include <rtdevice.h>
#include <string.h>
#include <stdio.h>

#ifdef RT_USING_PIN
static void pinm(int argc, char **argv)
{
    int pin;
    int m = PIN_MODE_INPUT;

    if (argc < 3)
    {
        printf("pinm pa0 o\r\n");
        printf("pinm pa0 i\r\n");
        return;
    }

    pin = rt_pin_get(argv[1]);
    if (pin < 0)
    {
        printf("can't get pin(%s)\r\n", argv[1]);
        return;
    }

    if (strcmp(argv[2], "o") == 0)
    {
        m = PIN_MODE_OUTPUT;
    }

    rt_pin_mode(pin, m);
}
MSH_CMD_EXPORT(pinm, pin mode set);

static void pinw(int argc, char **argv)
{
    int pin;
    int v;

    if (argc < 3)
    {
        printf("pinw pa0 0\r\n");
        printf("pinw pa0 1\r\n");
        return;
    }

    pin = rt_pin_get(argv[1]);
    if (pin < 0)
    {
        printf("can't get pin(%s)\r\n", argv[1]);
        return;
    }

    v = argv[2][0] - '0';
    rt_pin_write(pin, v);
}
MSH_CMD_EXPORT(pinw, pin Write);
#endif
